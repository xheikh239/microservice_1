package com.example.demo.security;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;


@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter implements WebMvcConfigurer {

	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Autowired
	private DataSource dataSource;

	@Value("${spring.queries.users-query}")
	private String usersQuery;

	@Value("${spring.queries.roles-query}")
	private String rolesQuery;

	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.jdbcAuthentication()
				.usersByUsernameQuery(usersQuery)
				.authoritiesByUsernameQuery(rolesQuery)
				.dataSource(dataSource)
				.passwordEncoder(passwordEncoder());

		auth.ldapAuthentication()
				.userDnPatterns("uid={0},ou=people")
				.contextSource()
				.url("ldap://localhost:8389/dc=gct,dc=com,dc=tn")
				.and()
				.passwordCompare()
				.passwordEncoder(passwordEncoder())
				.passwordAttribute("userPassword");
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http
				.httpBasic()

				.and()
				.csrf() // csrf config starts here
				.ignoringAntMatchers(
						"/user/**",
						"/",
						"/api/v1/basicauth")
				.csrfTokenRepository(CookieCsrfTokenRepository
						.withHttpOnlyFalse());

		http
				.authorizeRequests()
				.antMatchers("/api/v1/basicauth/").permitAll()
				.antMatchers(HttpMethod.POST,"/user/req/add").permitAll()
				.antMatchers(HttpMethod.GET, "/user/req/list","/user/req/manage-requests/**").hasAnyAuthority("VALIDATOR")
				.antMatchers(HttpMethod.GET, "/user/manage-users/**").hasAnyAuthority("ADMIN")
				.antMatchers(HttpMethod.DELETE,"/user/req/delete/{id}").hasAnyAuthority("ADMIN")
				.antMatchers(HttpMethod.PUT,"/user/req/update/{id}").hasAnyAuthority("ADMIN")
				.antMatchers("/index.html", "/", "/login").permitAll().anyRequest().authenticated()


				.and()
				.sessionManagement()
				.sessionCreationPolicy(SessionCreationPolicy.IF_REQUIRED)
				.sessionFixation().migrateSession()

				.and()
				.formLogin().disable().cors()

				.and().logout().invalidateHttpSession(true).deleteCookies("JSESSIONID");

		//Use HTTPS in Production
		http
				.requiresChannel().anyRequest().requiresSecure();
		// CSP to prevent XSS attacks
		http
				.headers().xssProtection().and().contentSecurityPolicy("script-src 'self'");

	}



	//secure cookies
	public void setCookie(HttpServletResponse response) {

		Cookie cookie = new Cookie("JSESSIONID", "cookies");
		cookie.setMaxAge(3600);    //sets expiration after one minute
		cookie.setSecure(true);
		cookie.setHttpOnly(true); //encryption for user’s session data

	}

	@Override
	public void addCorsMappings(CorsRegistry registry) {
		registry.addMapping("/**")
				.allowedOrigins("*")
				.allowedMethods("*");
	}

}
