package com.example.demo.service;

import com.example.demo.model.User;
import com.example.demo.repository.UserRepository;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service(value = "UserService")
public class UserService {

    private final UserRepository userRepository;

    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    public UserService(UserRepository userRepository, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userRepository = userRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    public User getUser() {
        return this.findUserByMatricule(SecurityContextHolder.getContext().getAuthentication().getName());

    }

    public User findUserByMatricule(String matricule) {
    return userRepository.findByMatricule(matricule);
    }

    public void saveUser(User user){
            user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
            user.setActive(false);
            userRepository.save(user);   
            }
    
    public List<User> getUsers() {

    return userRepository.findAllByOrderById();
        }

    public User getUserById(int id) {

    return userRepository.findById(id);
}

    public void deleteUser(int id) {
        userRepository.deleteById(id);
}  
    
    public void blockUser(int id) {
    	userRepository.blockUser(id);
    }

    public void unBlockUser(int id) {
        userRepository.unBlockUser(id);
        }

}


