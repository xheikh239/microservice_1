package com.example.demo.model;
//""""""""""""""""""""""""""users (admin and validators)
//""""""""""""""""""""""""""otherusers (ldap users)

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotEmpty;

import org.hibernate.validator.constraints.Length;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Entity
@Table(name = "user")
public class User  implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	
    @Column(name = "id")
    private Integer id;
    @Column(name = "matricule")
    private String matricule; //added to UserDetails
	
    @Column(name = "username")
    private String userName; //admin and validator users  
	
    @NotEmpty(message = "Password cannot be empty")
    @Length(min = 5, message = " must be 4 characters at least")
    @Column(name = "password")
    private String password;

    @Column(name = "role")
    private String roles;

    @Column(name = "Active")
	private boolean Active;


}
