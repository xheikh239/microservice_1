package com.example.demo.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.CreationTimestamp;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "demande_conge")
public class RequestForm implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    
    @Column(name = "matricule")
    private String Matricule;
    
    @Column(name = "username")
    private String userName;
    
    @NotEmpty(message = "email cannot be empty")
    @Email(message = "Please enter a valid email address", regexp = "^(.+)@(.+)$")
    private String email;

    @NotEmpty
    @Column(name = "Raison")
    private String raison;
    
    @Column(name = "dateDebut")
	  @DateTimeFormat(pattern = "yyyy-MM-dd")
	  private LocalDate dateDebut;
	
	  @Column(name = "dateFin")
	  @DateTimeFormat(pattern = "yyyy-MM-dd")
	  private LocalDate dateFin;

    @AssertTrue(message = "invalid date")
    private boolean isValidEndDate() {
      return dateFin != null && !dateFin.isBefore(dateDebut);
    }
    
    @Column(name = "duration")
    private Long duration;
    
    @Column(name = "active")
    private boolean active;

    @Column(name = "accept")
    private boolean accept;

    @CreationTimestamp
    @Column(name = "Created_At")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDateTime createdAt;

}
