package com.example.demo.controller;

import java.util.List;

import com.example.demo.model.User;
import com.example.demo.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@CrossOrigin(origins = "http://192.168.99.127:30170")
@RestController
public class AdminController {
 
    @Autowired
    UserService userService;

    @RequestMapping(value="/admin/manage-users", method=RequestMethod.GET)
    public List<User> ManageUsers() {

        return userService.getUsers();
    }

    
    @RequestMapping(value="/admin/manage-users/{action}/{id}", method=RequestMethod.GET)
    public void manageUsers(@PathVariable(value ="action") String action, @PathVariable(value = "id") int id) {

    if (action.equals("block")){
        userService.blockUser(id);
    }
    else if (action.equals("unblock")) {
        userService.unBlockUser(id);
    }
    
}

}




