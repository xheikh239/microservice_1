

INSERT INTO user(active, matricule, password, role, username)
VALUES (1,'92606','$2a$12$p3hwW5QZZ9Eif7i1l8vjCugPvEp92H9k36vrOICeq/xqCP24/FNM2','VALIDATOR','bedoui'),
(1,'92601','$2a$12$p3hwW5QZZ9Eif7i1l8vjCugPvEp92H9k36vrOICeq/xqCP24/FNM2','ADMIN','cheikh'),
(1,'92602','$2a$12$p3hwW5QZZ9Eif7i1l8vjCugPvEp92H9k36vrOICeq/xqCP24/FNM2','EMPLOYEE','92606'),
(0,'92603','$2a$12$p3hwW5QZZ9Eif7i1l8vjCugPvEp92H9k36vrOICeq/xqCP24/FNM2','ADMIN','admin1');

