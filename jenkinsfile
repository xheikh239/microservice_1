pipeline {
    agent any
    tools{
        maven 'Maven3'
        jdk 'jdk11'
        }

    environment {
        registry = "2gre82b4go10/microservice-1"
        registryCredential = 'DockerHub-login-details'
        dockerImage = ''

        NEXUS_VERSION = "nexus3"
        NEXUS_PROTOCOL = "http"
        NEXUS_URL = "192.168.1.23:8089"
        NEXUS_REPOSITORY = "maven-nexus-repo"
        NEXUS_CREDENTIAL_ID = "nexus-user-credentials"
    }

    stages {
        stage("clone") {
            steps   {
            git credentialsId: 'Gitlab_SSH_Auth', url: 'git@gitlab.com:xheikh239/microservice_1.git'
                    }

            post{
                success {
                    echo 'Successfully Cloned from Gitlab'
                    script  {
                        jiraComment body: '[Microservice-1] Successfully Cloned from Gitlab', issueKey: 'DEV-1'
                    }
                }

                failure {
                    echo 'Failed to clone'
                    script  {
                        jiraComment body: '[Microservice-1] FAILED to clone', issueKey: 'DEV-1'
                    }
                }
            }
        }
            
        stage("Build")  {
            steps   {
                sh "mvn clean package"
                    }

            post    {
                success {
                    echo 'successfully built'
                    script  {
                        jiraComment body: '[Microservice-1] Successfully built with maven', issueKey: 'DEV-2'
                    }
                }

                failure {
                    echo 'build failed'
                    script  {
                        jiraComment body: '[Microservice-1] FAILED to build with maven', issueKey: 'DEV-2'
                    }
                }
            }
        }
     
        stage("build & SonarQube analysis") {
            steps {
                withSonarQubeEnv('SonarQube') {
                sh 'mvn clean package sonar:sonar'
                }
            }
            
            post{
                success {
                    echo 'SonarQube analysis SUCCEED'
                    script{
                        jiraComment body: '[Microservice-1] SonarQube analysis SUCCEED', issueKey: 'DEV-4'
                    }
                }

                failure {
                    echo 'SonarQube analysis FAILED'
                    script{
                        jiraComment body: '[Microservice-1] SonarQube analysis FAILED', issueKey: 'DEV-4'
                    }
                }
            }
        }
            
        stage("Quality Gate") {
            steps {
                timeout(time: 1, unit: 'HOURS') {
                waitForQualityGate abortPipeline: true
                }
            }

            post{
                success {
                    echo 'Sonar QualityGate SUCCEED'
                    script{
                        jiraComment body: '[Microservice-1] Sonar QualityGate SUCCEED', issueKey: 'DEV-5'
                    }
                }

                failure {
                    echo 'Sonar QualityGate FAILED'
                    script{
                        jiraComment body: '[Microservice-1] Sonar QualityGate FAILED', issueKey: 'DEV-5'
                    }
                }

                aborted {
                    echo 'Pipeline ABORTED due to quality gate failure'
                    script{
                        jiraComment body: '[Microservice-1] Pipeline ABORTED due to quality gate failure', issueKey: 'DEV-5'
                    }
                }
            }

        }

        stage("Publish to Nexus Repository Manager") {
            steps {
                script  {
                    pom = readMavenPom file: "pom.xml";
                    filesByGlob = findFiles(glob: "target/*.${pom.packaging}");
                    echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
                    artifactPath = filesByGlob[0].path;
                    artifactExists = fileExists artifactPath;
                    if(artifactExists) {
                        echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${pom.packaging}, version ${pom.version}";
                        nexusArtifactUploader(

                            nexusVersion: NEXUS_VERSION,
                            protocol: NEXUS_PROTOCOL,
                            nexusUrl: NEXUS_URL,
                            groupId: pom.groupId,
                            version: pom.version,
                            repository: NEXUS_REPOSITORY,
                            credentialsId: NEXUS_CREDENTIAL_ID,
                            artifacts: [
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: artifactPath,
                                type: pom.packaging],
                                [artifactId: pom.artifactId,
                                classifier: '',
                                file: "pom.xml",
                                type: "pom"]
                            ]
                        );
                    } else {
                        error "*** File: ${artifactPath}, could not be found";
                    }
                }
            }
            
            post{
                success {
                    echo 'Artifacts Published With Successful'
                    script  {
                        jiraComment body: '[Microservice-1] Artifacts Published With Successful', issueKey: 'DEV-3'
                    }
                }

                failure {
                    echo 'Failed to publish Artifacts'
                    script{
                        jiraComment body: '[Microservice-1] Failed to publish Artifacts', issueKey: 'DEV-3'
                    }
                }
            }
        }

        stage('Building docker image') {
            steps {
                script {
                dockerImage = docker.build registry + ":latest"
            }
        }
            post{
                success {
                    echo 'docker image built with SUCCESSFUL'
                    script{
                        jiraComment body: '[Microservice-1] docker image built with SUCCESSFUL', issueKey: 'DEV-6'
                    }
                }

                failure {
                    echo 'FAILED to build docker image'
                    script{
                        jiraComment body: '[Microservice-1] FAILED to build docker image', issueKey: 'DEV-6'
                    }
                }
            }
    }


        stage('Anchore Scanner') {
            options {
              timeout(time: 1, unit: 'HOURS')   // timeout on this stage
          }
          
        steps {
              sh 'echo "2gre82b4go10/microservice-1:latest" ${WORKSPACE}/Dockerfile > anchore_images' 
              anchore engineCredentialsId: 'anchore-credentials', engineRetries: '30000', engineurl: 'http://localhost:8228/v1', forceAnalyze: true, name: 'anchore_images'
        }

          }
    
        stage('Deploying to dockerHub') {
            steps {
                script {
                    docker.withRegistry( '', registryCredential ) {
                    dockerImage.push()
                }
            }
        }
            post{
                success {
                    echo 'docker image Pushed to DockerHub'
                    script{
                        jiraComment body: '[Microservice-1] docker image Pushed to DockerHub', issueKey: 'DEV-7'
                    }
                }

                failure {
                    echo 'FAILED to Push tthe image'
                    script{
                        jiraComment body: '[Microservice-1] FAILED to Push tthe image', issueKey: 'DEV-7'
                    }
                }
            }
    }
        //stage('Cleaning up') {
          //  steps {
            //    sh "docker rmi $registry:latest"
            //}
        //}
    }
}
