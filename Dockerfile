FROM openjdk:11-jre-slim
EXPOSE 8081
ADD target/demande_conge-0.1.jar demande_conge-0.1.jar
ENTRYPOINT ["java", "-jar","demande_conge-0.1.jar"]
